package hello;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class User {

    private String surname;
    private String name;
    private int age;
    private int height;
    @Id
    private ObjectId _id;

    public User() {

    }

    public User(ObjectId _id, String name, String surname, int age, int height) {
        this._id = _id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.height = height;
    }

    public User(String name, String surname, int age, int height) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.height = height;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String username) {
        this.surname = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }


    public String get_id() { return _id.toHexString(); }

    public void set_id(ObjectId _id) { this._id = _id; }


}
