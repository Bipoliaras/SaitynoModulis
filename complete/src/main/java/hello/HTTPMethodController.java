package hello;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.fasterxml.jackson.core.json.*;
import org.springframework.web.util.UriComponentsBuilder;


@RestController
public class HTTPMethodController {

    @Autowired
    private UserRepository repo;


    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException {
        @Override
        public String getMessage() {
            return "User not found";
        }
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getuser(@PathVariable("id") ObjectId id) {

        if(repo.findBy_id(id) != null) {
            return new ResponseEntity<>(repo.findBy_id(id), HttpStatus.OK);
        } else {
            return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
        }


    }

    @RequestMapping(value = "/user/", method = RequestMethod.GET)
    public ResponseEntity<?> getUsers() {

        List<User> list = repo.findAll();

        if (list == null) {
            return new ResponseEntity(new CustomErrorType("Users not found"),
                    HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<List<User>>(list, HttpStatus.OK);
    }


/*
    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    @ResponseBody
    public void POSThandler(
            @RequestParam("name") String name, @RequestParam("surname") String surname, @RequestParam("age") int age,
    @RequestParam("height") int height) {

            repo.save(new User(userId,name,surname,age,height));
            userId++;

    } */

    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT, consumes = "application/json",
            produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> handlePut(@PathVariable("id") ObjectId id, @RequestBody String json) {

        if(repo.findBy_id(id) == null || id.toHexString() == null || id == null || ObjectId.isValid(String.valueOf(id)))  {
            return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
        }

            try {
                //User newUser = new User();
                ObjectMapper mapper = new ObjectMapper();
                User newUser = mapper.readValue(json, User.class);
                newUser.set_id(id);
                repo.save(newUser);

                return new ResponseEntity<HttpStatus>(HttpStatus.OK);

            }
            catch (JsonParseException ex) {
                return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
            }
            catch (JsonMappingException ex) {
                return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
            }
            catch (IOException ex) {
                return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);

            }

    }

    @RequestMapping(value = "/user/", method = RequestMethod.POST, produces="application/json", consumes="application/json")
    @ResponseBody
    public ResponseEntity<?>  post(@RequestBody String json) {

        User newUser = new User();

        try {
            ObjectMapper mapper = new ObjectMapper();
            newUser = mapper.readValue(json, User.class);
            newUser.set_id(ObjectId.get());
            repo.save(newUser);
        }
        catch (JsonGenerationException ex)  {

        }
        catch (JsonMappingException ex)  {

        }
        catch (IOException ex) {

        }

        if(newUser.getName() != null && newUser.get_id() != null && newUser.getSurname() != null &&
                newUser.getHeight() > 0 && newUser.getAge() > 0) {
            return new ResponseEntity<User>(newUser, HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity<HttpStatus>(HttpStatus.BAD_REQUEST);
        }



    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> DeleteHandler(@PathVariable("id") ObjectId id) {

        User deleteUser = repo.findBy_id(id);
        if(deleteUser != null) {
            repo.delete(deleteUser);
            return new ResponseEntity<HttpStatus>(HttpStatus.OK);
        } else {
            return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT );
        }




    }




}
